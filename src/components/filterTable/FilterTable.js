import { Container, Grid, TextField, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";

var FilterCheck = '';

const Row = [ 'Cà phê', 'Trà tắc', 'Pepsi', 'Cocacola', 'Trà sữa', 'Matcha', 'Hồng trà', 'Trà xanh kem cheese', 'Trà đá' ]
function FilterTable() {
    const dispatch = useDispatch();
    const { taskNameInput } = useSelector((reduxData) => reduxData.taskReducer);

    const inputChangeHandler = (event) => {
        dispatch({
            type: "TASK_INPUT_CHANGE",
            value: event.target.value
        })
    }

    const filterTableClick = () => {
        console.log("Lọc thông tin")
        FilterCheck = taskNameInput.trim();
        dispatch({
            type: "FILTER_USE",
            value: taskNameInput
        })
        console.log(Row.filter(row => row.toLowerCase().includes(FilterCheck.toLowerCase())));
    }

    return (
        <Container>
            <Grid container mt={3}>
                <Grid xs={10} md={10} lg={10} sm={12} item>
                    <TextField fullWidth placeholder="Input Task name" variant="outlined" label="Nhập nội dung dòng " onChange={inputChangeHandler} />
                </Grid>
                <Grid xs={2} md={2} lg={2} sm={12} item textAlign={"center"}>
                    <Button variant="contained" style={{ marginTop: "3px", width: "150px", height: "50px" }} onClick={filterTableClick}>Lọc</Button>
                </Grid>
            </Grid>
            <br></br>
            <Grid>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 500 }} aria-label="simple table">
                        <TableHead>
                            <TableRow className="text-light" style={{ backgroundColor: "peachpuff" }}>
                                <TableCell align="center" style={{fontWeight: "bold"}}>STT</TableCell>
                                <TableCell align="center" style={{fontWeight: "bold"}}>Nội dung</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {Row.filter(row => row.toLowerCase().includes(FilterCheck.toLowerCase())).map((element, index) => (
                                <TableRow key={index}>
                                    <TableCell component="th" align="center" scope="row">
                                        {index + 1}
                                    </TableCell>
                                    <TableCell align="center">{element}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </Container>
    )
}

export default FilterTable;
