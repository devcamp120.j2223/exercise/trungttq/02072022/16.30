const initialState = {
    taskNameInput: "",
    table: [],
    FilterCheck: "",
}

const FilterTableEvents = (state = initialState, action) => {
    switch (action.type) {
        case "TASK_INPUT_CHANGE":
            return {
                taskNameInput: action.value,
                table: state.table
            }
        case "FILTER_USE":
            return {
                FilterCheck: action.value,
                table: state.table
            }
        default:
            return state;
    }
}

export default FilterTableEvents;


