import { createStore, combineReducers } from "redux";
import FilterTableEvents from "../components/filterTable/FilterTableReducer";

const appReducer = combineReducers({
    taskReducer: FilterTableEvents
});

const store = createStore(appReducer);

export default store;
