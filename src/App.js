import FilterTable from "./components/filterTable/FilterTable";

function App() {
  return (
    <div>
      <FilterTable/>
    </div>
  );
}

export default App;
